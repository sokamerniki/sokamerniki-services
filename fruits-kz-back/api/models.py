from django.db import models

# Create your models here.


class Fruit(models.Model):
    name = models.CharField(max_length=250)
    price = models.FloatField()
    image = models.ImageField(upload_to='media')

    def __str__(self):
        return self.name


class Order(models.Model):
    total_price = models.FloatField()
    products_count = models.TextField()
    order_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.products_count} | {self.total_price} | {self.order_time}"


class Analytics(models.Model):
    total_purchase_count = models.IntegerField()
    total_money = models.FloatField()
    analytic_time = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return f"{self.total_purchase_count} | {self.total_money} | {self.analytic_time}"