from rest_framework import serializers
from .models import Fruit, Order


class FruitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Fruit
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['products_count', 'total_price']