from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from .models import Fruit
from .serializer import FruitSerializer, OrderSerializer
# from django.shortcuts import render
from rest_framework.response import Response


# Create your views here.

class FruitAPIView(generics.ListAPIView):
    queryset = Fruit.objects.all()
    serializer_class = FruitSerializer


class FruitAPIDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Fruit.objects.all()
    serializer_class = FruitSerializer


@api_view(['POST'])
def add_order(request):
    if(request.data.get('fruits') == ''):
        return Response("Error")
    serializer_class = OrderSerializer(data=request.data)
    if serializer_class.is_valid():
        serializer_class.save()
    else:
        print(request.data)
        print('I am here')
    
    return Response("Hello World")