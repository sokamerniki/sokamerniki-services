job "sokamerniki" {
  datacenters = ["dc1"]

  group "sokamerniki-group" {
    count = 1

    task "etl-task" {
      driver = "docker"

      config {
        image = "yrssl/etl-job:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 500
        memory = 256
      }

      service {
        name = "example-service"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "2s"
        }
      }

      task "backend-task" {
      driver = "docker"

      config {
        image = "yrssl/fruits-kz-back:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 500
        memory = 256
      }

      service {
        name = "example-service"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
