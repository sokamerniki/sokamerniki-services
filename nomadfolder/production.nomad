job "production-job" {
  datacenters = ["dc1"]

  group "production-group" {
    count = 3  # Higher count for production scalability

    task "production-etl-task" {
      driver = "docker"

      config {
        image = "yrssl/etl-job:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 1000
        memory = 512
      }

      service {
        name = "production-service"
        port = "http"

        check {
          type     = "http"
          path     = "/health"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
    task "production-backend-task" {
      driver = "docker"

      config {
        image = "yrssl/fruits-kz-back:latest"
        ports = ["http"]
      }

      resources {
        cpu    = 1000
        memory = 512
      }

      service {
        name = "production-service"
        port = "http"

        check {
          type     = "http"
          path     = "/health"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
