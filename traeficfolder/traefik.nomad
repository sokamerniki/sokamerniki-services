job "traefik" {
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    task "traefik" {
      driver = "docker"

      config {
        image = "traefik:v2.5"
        ports = ["80:80", "8080:8080"]

        volumes = [
          "/traefikfolder/config:/etc/traefik",
        ]
      }

      template {
        data = <<EOF
          # Additional dynamic configuration if needed
        EOF

        destination = "local/config/dynamic.toml"
      }

      resources {
        cpu    = 500 # adjust as needed
        memory = 256 # adjust as needed
      }
    }
  }
}
