import psycopg2
import re
import time
from datetime import datetime, timedelta

db_params = {
    'dbname': '$DB_NAME',
    'user': '$DB_USER',
    'password': '$DB_PASSWORD',
    'host': 'DB_HOST', 
    'port': 'DB_PORT'
}

config = psycopg2.connect(**db_params)
current = config.cursor()

def extract():
    data = []
    end = datetime.now()
    start = end - timedelta(minutes=1)

    select = "SELECT products_count, total_price FROM api_order WHERE order_time >= %s AND order_time <= %s;"
    current.execute(select, (start, end))
    data = current.fetchall()
    config.commit()

    return data

    
def transform(data):
    total_purchase, total_money = 0, 0 
    for orders, cost in data:
        numbers = []

        for order in orders:
            found_numbers = re.findall(r'\d+', order)
            numbers.extend(found_numbers)

        numbers = [int(num) for num in numbers]

        total_purchase += sum(numbers)
        total_money += cost
    
    return [total_purchase, total_money]

def load(data):
    time_of_load = datetime.now()
    total_purchase, total_money = data
    insert = f"INSERT INTO api_analytics (total_purchase_count, total_money, analytic_time) VALUES ({total_purchase}, {int(total_money)}, '{time_of_load}');"
    current.execute(insert)
    config.commit()

while True:
    temp = extract()
    ans = transform(temp)
    load(ans)
    print(f"job passed for {datetime.now()}")
    time.sleep(60)

current.close()
config.close()
