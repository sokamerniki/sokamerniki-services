# Sokamerniki-services


## Description
Small unfinished project.


## INSTALL 
-git clone 
run the Dockerfile 

## Functionality
Functionality
extract(): Retrieves order data from the api_order table for the last minute.
transform(data): Processes the retrieved data to calculate the total number of purchases and the total money spent.
load(data): Inserts the processed data into the api_analytics table.
