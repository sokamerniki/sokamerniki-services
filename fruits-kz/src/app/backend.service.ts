import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Product } from './models/products';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private baseURL = 'http://127.0.0.1:8000/api/'
  constructor(private http: HttpClient) { }

  getFruits(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseURL + 'fruits/');
  }

  postOrder(total_price: number, products_count: string): any {
    return this.http.post(this.baseURL + 'add/order/', {total_price, products_count}).subscribe(
      (error) => {
        console.error(error)
      }
    )
  }

}
