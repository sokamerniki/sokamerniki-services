export interface Product {
    id: number;
    name: string;
    price: number;
    image: string;
    count?: number;
}

export const products = [
    {
        id: 1,
        name: 'Банан',
        price: 800,
        image: 'assets/images/banana.png'
    },
    {
        id: 2,
        name: 'Апельсин',
        price: 1200,
        image: 'assets/images/orange.webp'
    },
    {
        id: 3,
        name: 'Яблоко',
        price: 600,
        image: 'assets/images/apple.png'
    },
    {
        id: 3,
        name: 'Ананас',
        price: 1500,
        image: 'assets/images/pineapple.png'
    },
    {
        id: 4,
        name: 'Персик',
        price: 920,
        image: 'assets/images/peach.png'
    },
    {
        id: 5,
        name: 'Абрикос',
        price: 850,
        image: 'assets/images/appricote.png'
    }
];

export var selected_fruits = [
    
]